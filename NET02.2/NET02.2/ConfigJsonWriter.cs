﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using Newtonsoft.Json;


namespace NET02._2
{
    /// <summary>
    /// The class for writing to json file.
    /// </summary>
    public class ConfigJsonWriter
    {
        private string _path;
        private readonly Regex _pattern = new Regex(@"(.*(?:\\|\/)+)?");

        public string Path
        {
            get => _path;
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }

                if ((_path = _pattern.Match(value).Value) == string.Empty)
                {
                    throw new ArgumentException("Invalid file path");
                }
            }
        }

        public ConfigJsonWriter(string path)
        {
            Path = path;
        }

        /// <summary>
        /// The method writes user login information to json file.
        /// </summary>
        /// <param name="loginConfig">Login configuration file.</param>
        public void Write(LoginConfig loginConfig)
        {
            if (loginConfig == null)
            {
                throw new ArgumentNullException();
            }

            loginConfig.CorrectedWindowsParams();

            foreach (var login in loginConfig.Logins)
            {
                using (var stream = new StreamWriter(_path + AddFileExtension(login.UserName)))
                {
                    var json = JsonConvert.SerializeObject(login, Formatting.Indented);
                    stream.Write(json);
                }
            }
        }

        private string AddFileExtension(string login) => login + ".json";
    }
}