﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NET02._2
{
    /// <summary>
    /// The class to describe the login info configuration.
    /// </summary>
    public class LoginConfig
    {
        private readonly List<LoginInfo> _logins;

        public IEnumerable<LoginInfo> Logins => _logins;

        public IEnumerable<LoginInfo> IncorrectLogins => _logins.Where(l => !l.IsCorrect());

        public LoginConfig(List<LoginInfo> logins)
        {
            _logins = logins ?? throw new ArgumentNullException(nameof(logins));
        }

        /// <summary>
        /// The method corrected windows params.
        /// </summary>
        public void CorrectedWindowsParams()
        {
            foreach (var loginInfo in Logins)
            {
                var windowsParams = loginInfo.WindowsParams.ToList();
                foreach (var windowsParam in windowsParams)
                {
                    WindowParams.Correct(windowsParam);
                }
            }
        }

        /// <summary>
        /// The method returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString() => string.Join(null, _logins);
    }
}