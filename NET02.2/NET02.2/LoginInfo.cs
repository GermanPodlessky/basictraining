﻿using System;
using System.Collections.Generic;

namespace NET02._2
{
    /// <summary>
    /// The class describes the parameters of windows in the user login.
    /// </summary>
    public class LoginInfo
    {
        private List<WindowParams> _windowsParams { get; }

        public string UserName { get; }
        public IEnumerable<WindowParams> WindowsParams => _windowsParams;

        public LoginInfo(string name, List<WindowParams> windowsParams)
        {
            UserName = name ?? throw new ArgumentNullException(nameof(name));
            _windowsParams = windowsParams ?? throw new ArgumentNullException(nameof(windowsParams));
        }

        /// <summary>
        /// The method verifies the correctness of the login.
        /// </summary>
        /// <returns>True if the login correct, otherwise - false.</returns>
        public bool IsCorrect()
        {
            return _windowsParams.Exists(w => w.Title == "main" && w.IsCorrect);
        }

        /// <summary>
        /// The method returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return $"Login: {UserName}{Environment.NewLine}{WindowsParamsToString()}";
        }

        /// <summary>
        /// The method Converts a collection to a string.
        /// </summary>
        /// <returns>Row with collection items.</returns>
        private string WindowsParamsToString()
        {
            return string.Join(Environment.NewLine, WindowsParams);
        }
    }
}