﻿namespace NET02._2
{
    /// <summary>
    /// The class describes the window params.
    /// </summary>
    public class WindowParams
    {
        private const string NoneSymbol = "?";
        private static WindowParams _defaultWindowParams => new WindowParams("default", 0, 0, 400, 150);

        public bool IsCorrect { get; }
        public string Title { get; }
        public uint? Top { get; }
        public uint? Left { get; }
        public uint? Width { get; }
        public uint? Height { get; }

        public WindowParams(string title, uint? top, uint? left, uint? width, uint? height)
        {
            Title = title;
            Top = top;
            Left = left;
            Width = width;
            Height = height;
            IsCorrect = !ToString().Contains(NoneSymbol);
        }

        public static void Correct(WindowParams windowParams)
        {
            if (!windowParams.IsCorrect)
            {
                windowParams = _defaultWindowParams;
            }
        }

        /// <summary>
        /// The method returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public sealed override string ToString()
        {
            return $"{Title}({WindowValueToSting(Top)}, {WindowValueToSting(Left)}," +
                   $" {WindowValueToSting(Width)}, {WindowValueToSting(Height)})";
        }

        /// <summary>
        /// The method converts input value to string.
        /// </summary>
        /// <param name="value">Window value.</param>
        /// <returns>Window value in string format.</returns>
        private string WindowValueToSting(uint? value)
        {
            return value.HasValue ? value.Value.ToString() : NoneSymbol;
        }
    }
}