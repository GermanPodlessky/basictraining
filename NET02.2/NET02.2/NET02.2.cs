﻿using System;
using System.IO;

namespace NET02._2
{
    class Net02_2
    {
        static void Main(string[] args)
        {
            var path = @"..\..\Config\TestFile.xml";

            try
            {
                if (!File.Exists(path))
                {
                    throw new FileNotFoundException();
                }

                var config = new XmlReader(path).GetLoginConfig();

                foreach (var login in config.Logins)
                {
                    Console.WriteLine(login);
                }

                Console.WriteLine();

                foreach (var login in config.IncorrectLogins)
                {
                    Console.WriteLine(login);
                }

                new ConfigJsonWriter(path).Write(config);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}