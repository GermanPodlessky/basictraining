﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace NET02._2
{
    /// <summary>
    /// The class for reading from XML file.
    /// </summary>
    public class XmlReader
    {
        private readonly Regex _pattern = new Regex(@"(.*(?:\\|\/)+)?((.*)(\.([^?\s]*)))\??(.*)?");
        private string _path;

        public string Path
        {
            get => _path;
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }

                if ((_path = _pattern.Match(value).Value) == string.Empty)
                {
                    throw new ArgumentException("Invalid file path");
                }

                _path = value;
            }
        }

        public XmlReader(string path)
        {
            Path = path;
        }

        /// <summary>
        /// The method reads the login information of the user from the XML file.
        /// </summary>
        public LoginConfig GetLoginConfig()
        {
            var xDoc = XDocument.Load(_path);

            return XmlParser(xDoc.Root);
        }

        /// <summary>
        /// Converts the xml file to config class object.
        /// </summary>
        /// <param name="xElement">Represents an XML element.</param>
        /// <returns>Configuration from xml file.</returns>
        public static LoginConfig XmlParser(XElement xElement)
        {
            var logins = xElement?.Elements()
                .Select(node => new {node, login = node.Attribute("name")})
                .Select(x =>
                    new LoginInfo
                    (
                        x.login?.Value ?? "?",
                        x.node.Elements().Select(t =>
                                new
                                {
                                    Name = t.Attribute("title"),
                                    Top = t.Element("top"),
                                    Left = t.Element("left"),
                                    Width = t.Element("width"),
                                    Height = t.Element("height")
                                })
                            .Select(t =>
                                new WindowParams(
                                    t.Name?.Value ?? "?",
                                    t.Top == null ? default(uint?) : uint.Parse(t.Top.Value),
                                    t.Left == null ? default(uint?) : uint.Parse(t.Left.Value),
                                    t.Width == null ? default(uint?) : uint.Parse(t.Width.Value),
                                    t.Height == null ? default(uint?) : uint.Parse(t.Height.Value)
                                )
                            ).ToList()
                    )).ToList();

            return new LoginConfig(logins);
        }
    }
}