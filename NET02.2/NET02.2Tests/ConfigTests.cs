﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace NET02._2.Tests
{
    [TestClass]
    public class ConfigTests
    {
        private XElement xElement = new XElement("config",
            new XElement("login", new XAttribute("name", "alex"),
                new XElement("window", new XAttribute("title", "main"),
                    new XElement("top", 10),
                    new XElement("left", 20),
                    new XElement("width", 400),
                    new XElement("height", 200)),
                new XElement("window", new XAttribute("title", "help"),
                    new XElement("top", 20),
                    new XElement("left", 30),
                    new XElement("height", 100)),
                new XElement("window", new XAttribute("title", "search"),
                    new XElement("left", 47),
                    new XElement("width", 377),
                    new XElement("height", 235))
            ),
            new XElement("login", new XAttribute("name", "user1"),
                new XElement("window", new XAttribute("title", "main"),
                    new XElement("top", 10),
                    new XElement("left", 20),
                    new XElement("width", 400)),
                new XElement("window", new XAttribute("title", "help"),
                    new XElement("top", 20),
                    new XElement("left", 30)
                )),
            new XElement("login", new XAttribute("name", "user2"),
                new XElement("window", new XAttribute("title", "main"),
                    new XElement("top", 10),
                    new XElement("left", 20),
                    new XElement("width", 400),
                    new XElement("height", 100)),
                new XElement("window", new XAttribute("title", "help"),
                    new XElement("top", 20),
                    new XElement("left", 30)
                )));

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SetLoginsNull()
        {
            new LoginConfig(null);
        }

        [TestMethod]
        public void SetCorrectLogins()
        {
            // Arrange
            var login = new LoginInfo("Test", new List<WindowParams> {new WindowParams("Test", 0, 0, 0, 0)});
            var config = new LoginConfig(new List<LoginInfo> {login});
            var expected = login;

            // Act
            var actual = config.Logins.ToList()[0];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IncorrectLogins()
        {
            // Arrange
            var config = XmlReader.XmlParser(xElement);

            // Act
            foreach (var str in config.IncorrectLogins)
            {
                var condition = str.ToString().Contains('?');
                Assert.IsTrue(condition);
            }
        }
    }
}