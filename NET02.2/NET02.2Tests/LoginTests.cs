﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NET02._2.Tests
{
    [TestClass]
    public class LoginTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SetNameNull()
        {
            new LoginInfo(null, new List<WindowParams> {new WindowParams("Test", 0, 0, 0, 0)});
        }

        [TestMethod]
        public void SetCorrectName()
        {
            // Arrange
            var login = new LoginInfo("Test", new List<WindowParams>());
            var expected = "Test";

            // Act
            var actual = login.UserName;

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SetWindowsNull()
        {
            new LoginInfo("Test", null);
        }

        [TestMethod]
        public void SetCorrectWindows()
        {
            // Arrange
            var window = new WindowParams("Test", 0, 0, 0, 0);
            var login = new LoginInfo("Test", new List<WindowParams> {window});
            var expected = window;

            // Act
            var actual = login.WindowsParams.ToList()[0];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsCorrectTrue()
        {
            // Arrange
            var login = new LoginInfo("Test", new List<WindowParams>
                {
                    new WindowParams("main", 0, 0, 0, 0),
                    new WindowParams("Test", 0, 0, 0, 0)
                }
            );

            // Act
            var condition = login.IsCorrect();

            // Assert
            Assert.IsTrue(condition);
        }

        [TestMethod]
        public void IsCorrectFalse()
        {
            // Arrange
            var login = new LoginInfo("Test", new List<WindowParams>
                {
                    new WindowParams("main", null, 0, 0, 0),
                    new WindowParams("Test", 0, 0, 0, 0)
                }
            );

            // Act
            var condition = login.IsCorrect();

            // Assert
            Assert.IsFalse(condition);
        }
    }
}