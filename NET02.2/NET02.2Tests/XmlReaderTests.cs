﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Xml.Linq;

namespace NET02._2.Tests
{
    [TestClass]
    public class XmlReaderTests
    {
        private XElement xElement = new XElement("config",
            new XElement("login", new XAttribute("name", "alex"),
                new XElement("window", new XAttribute("title", "main"),
                    new XElement("top", 10),
                    new XElement("left", 20),
                    new XElement("width", 400),
                    new XElement("height", 200)),
                new XElement("window", new XAttribute("title", "help"),
                    new XElement("top", 20),
                    new XElement("left", 30),
                    new XElement("height", 100)),
                new XElement("window", new XAttribute("title", "search"),
                    new XElement("left", 47),
                    new XElement("width", 377),
                    new XElement("height", 235))
            ),
            new XElement("login", new XAttribute("name", "user1"),
                new XElement("window", new XAttribute("title", "main"),
                    new XElement("top", 10),
                    new XElement("left", 20),
                    new XElement("width", 400)),
                new XElement("window", new XAttribute("title", "help"),
                    new XElement("top", 20),
                    new XElement("left", 30)
                )),
            new XElement("login", new XAttribute("name", "user2"),
                new XElement("window", new XAttribute("title", "main"),
                    new XElement("top", 10),
                    new XElement("left", 20),
                    new XElement("width", 400),
                    new XElement("height", 100)),
                new XElement("window", new XAttribute("title", "help"),
                    new XElement("top", 20),
                    new XElement("left", 30)
                )));

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SetNullPath()
        {
            new XmlReader(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SetIncorrectPath()
        {
            new XmlReader("Test");
        }

        [TestMethod]
        public void SetCorrectPath()
        {
            // Arrange
            var path = @"..\..\Config\TestFile.xml";

            // Act
            new XmlReader(path);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void XmlParserNull()
        {
            XmlReader.XmlParser(null);
        }

        [TestMethod]
        public void XmlParser()
        {
            Assert.IsNotNull(XmlReader.XmlParser(xElement).Logins);
        }
    }
}