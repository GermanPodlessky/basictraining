﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NET02._2.Tests
{
    [TestClass]
    public class WindowTests
    {
        [TestMethod]
        public void IsCorrectTrue()
        {
            // Arrange
            var window = new WindowParams("Test", 0, 0, 0, 0);

            // Act
            var condition = window.IsCorrect;

            // Assert
            Assert.IsTrue(condition);
        }

        [TestMethod]
        public void IsCorrectFalse()
        {
            // Arrange
            var window = new WindowParams("Test", null, 0, 0, 0);

            // Act
            var condition = window.IsCorrect;

            // Assert
            Assert.IsFalse(condition);
        }
    }
}