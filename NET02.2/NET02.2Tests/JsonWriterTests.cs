﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace NET02._2.Tests
{
    [TestClass]
    public class JsonWriterTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SetNullPath()
        {
            new ConfigJsonWriter(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SetIncorrectPath()
        {
            new ConfigJsonWriter("Test");
        }

        [TestMethod]
        public void SetCorrectPath()
        {
            // Arrange
            var path = @"..\..\Config\TestFile.xml";

            // Act
            new ConfigJsonWriter(path);
        }
    }
}