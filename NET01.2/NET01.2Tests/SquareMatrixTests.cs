﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace NET01._2.Tests
{
    [TestClass]
    public class SquareMatrixTests
    {
        [TestMethod]
        public void GetElement()
        {
            // Arrange
            var squareMatrix = new SquareMatrix<int>(3, Enumerable.Range(0, 9).ToArray());
            var expected = 1;

            // Act
            var actual = squareMatrix[0, 1];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        private class Person
        {
        }

        [TestMethod]
        public void SetElement()
        {
            // Arrange
            var person = new Person();
            var squareMatrix = new SquareMatrix<Person>(2, null, null, null, null);
            var expected = person;

            // Act
            squareMatrix[0, 0] = person;
            var actual = squareMatrix[0, 0];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void MatrixInitializationNull()
        {
            new SquareMatrix<int>(0, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SetSizeLessThanZero()
        {
            new SquareMatrix<int>(5, 0, 1, 2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void OverflowMatrix()
        {
            new SquareMatrix<int>(1, 0, 1);
        }

        [TestMethod]
        public void AnonymousMethod()
        {
            // Arrange
            var squareMatrix = new SquareMatrix<int>(4, Enumerable.Range(0, 16).ToArray());
            var condition = false;
            squareMatrix.ElementChanged += delegate { condition = !condition; };

            // Act
            squareMatrix[2, 3] = 155;

            // Assert
            Assert.IsTrue(condition);
        }

        [TestMethod]
        public void ConventionalMethod()
        {
            // Arrange
            var squareMatrix = new SquareMatrix<object>(2, 0, 1, 2, 3);
            var condition = false;
            squareMatrix.ElementChanged += InformationAboutElement;

            // Act
            squareMatrix[1, 1] = "Test";

            // Assert
            Assert.IsTrue(condition);

            void InformationAboutElement<T>(object sender, ElementMatrixEventArgs<T> e)
            {
                condition = !condition;
            }
        }

        [TestMethod]
        public void LambdaExpression()
        {
            // Arrange
            var squareMatrix = new SquareMatrix<object>(2, 0, 1, 2, 3);
            var condition = false;
            squareMatrix.ElementChanged += (sender, e) => condition = !condition;

            // Act
            squareMatrix[1, 1] = "Test";

            // Assert
            Assert.IsTrue(condition);
        }
    }
}