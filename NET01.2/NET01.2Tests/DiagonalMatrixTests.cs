﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace NET01._2.Tests
{
    [TestClass]
    public class DiagonalMatrixTests
    {
        [TestMethod]
        public void GetElement()
        {
            // Arrange
            var diagonalMatrix = new DiagonalMatrix<int>(Enumerable.Range(0, 5).ToArray());
            var expected = 1;

            // Act
            var actual = diagonalMatrix[1, 1];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        private class Person
        {
        }

        [TestMethod]
        public void SetElement()
        {
            // Arrange
            var person = new Person();
            var diagonalMatrix = new DiagonalMatrix<Person>(null, null);
            var expected = person;

            // Act
            diagonalMatrix[0, 0] = person;
            var actual = diagonalMatrix[0, 0];

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void MatrixInitializationNull()
        {
            new DiagonalMatrix<int>(null);
        }

        [TestMethod]
        public void AnonymousMethod()
        {
            // Arrange
            var squareMatrix = new DiagonalMatrix<int>(Enumerable.Range(0, 16).ToArray());
            var condition = false;
            squareMatrix.ElementChanged += delegate { condition = !condition; };

            // Act
            squareMatrix[2, 2] = 155;

            // Assert
            Assert.IsTrue(condition);
        }

        [TestMethod]
        public void ConventionalMethod()
        {
            // Arrange
            var squareMatrix = new DiagonalMatrix<object>(0, 1, 2, 3);
            var condition = false;
            squareMatrix.ElementChanged += InformationAboutElement;

            // Act
            squareMatrix[1, 1] = "Test";

            // Assert
            Assert.IsTrue(condition);

            void InformationAboutElement<T>(object sender, ElementMatrixEventArgs<T> e)
            {
                condition = !condition;
            }
        }

        [TestMethod]
        public void LambdaExpression()
        {
            // Arrange
            var squareMatrix = new DiagonalMatrix<object>(2, 0, 1, 2, 3);
            var condition = false;
            squareMatrix.ElementChanged += (sender, e) => condition = !condition;

            // Act
            squareMatrix[1, 1] = "Test";

            // Assert
            Assert.IsTrue(condition);
        }
    }
}