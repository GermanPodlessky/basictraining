﻿using System;

namespace NET01._2
{
    /// <summary>
    /// The Class for describing information about the matrix element when generating an event.
    /// </summary>
    /// <typeparam name="T">Type of elements in the matrix.</typeparam>
    public class ElementMatrixEventArgs<T> : EventArgs
    {
        public uint I { get; }
        public uint J { get; }

        public ElementMatrixEventArgs(uint i, uint j)
        {
            I = i;
            J = j;
        }
    }
}