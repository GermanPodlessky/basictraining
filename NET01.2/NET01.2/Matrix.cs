﻿using System;

namespace NET01._2
{
    /// <summary>
    /// The base class to describe the matrix.
    /// </summary>
    /// <typeparam name="T">Type of elements in the matrix.</typeparam>
    public abstract class Matrix<T>
    {
        protected T[] Data;

        public virtual uint RowSize { get; protected set; }
        public virtual uint ColumnSize { get; }
        public event EventHandler<ElementMatrixEventArgs<T>> ElementChanged;

        public abstract T this[uint i, uint j] { get; set; }

        /// <summary>
        /// The method raises the event when the item changes.
        /// </summary>
        /// <param name="value">Event data.</param>
        protected virtual void OnElementChanged(ElementMatrixEventArgs<T> value)
        {
            ElementChanged?.Invoke(this, value);
        }

        /// <summary>
        /// The method checks indexes for correctness.
        /// </summary>
        /// <param name="i">Index "I".</param>
        /// <param name="j">Index "J"</param>
        protected void CheckIndexes(uint i, uint j)
        {
            if (i >= RowSize)
            {
                throw new IndexOutOfRangeException(nameof(i));
            }


            if (j >= ColumnSize)
            {
                throw new IndexOutOfRangeException(nameof(j));
            }
        }
    }
}