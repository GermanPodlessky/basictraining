﻿using System;

namespace NET01._2
{
    /// <summary>
    /// The Class for describing a diagonal matrix.
    /// </summary>
    /// <typeparam name="T">Type of elements in the matrix.</typeparam>
    public class DiagonalMatrix<T> : SquareMatrix<T>
    {
        public override T this[uint i, uint j]
        {
            get
            {
                CheckIndexes(i, j);
                return i == j ? Data[i] : default;
            }
            set
            {
                CheckIndexes(i, j);

                if (i != j)
                {
                    throw new ArgumentException("Item is not on the main diagonal.");
                }

                if (Equals(Data[i], value))
                {
                    return;
                }

                Data[i] = value;
                OnElementChanged(new ElementMatrixEventArgs<T>(i, j));
            }
        }

        public DiagonalMatrix(params T[] data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            Data = new T[data.Length];
            base.RowSize = (uint) Data.Length;
            Array.Copy(data, Data, data.Length);
        }
    }
}