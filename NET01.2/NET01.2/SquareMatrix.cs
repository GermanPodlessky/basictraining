﻿using System;

namespace NET01._2
{
    /// <summary>
    /// The Class for describing a square matrix.
    /// </summary>
    /// <typeparam name="T">Type of elements in the matrix.</typeparam>
    public class SquareMatrix<T> : Matrix<T>
    {
        public override uint RowSize { get; protected set; }
        public override uint ColumnSize => RowSize;

        public override T this[uint i, uint j]
        {
            get
            {
                CheckIndexes(i, j);
                return Data[i * RowSize + j];
            }
            set
            {
                CheckIndexes(i, j);

                if (Equals(Data[i * RowSize + j], value))
                {
                    return;
                }

                Data[i * RowSize + j] = value;
                OnElementChanged(new ElementMatrixEventArgs<T>(i, j));
            }
        }

        public SquareMatrix(uint size, params T[] data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            if (data.Length != size * size)
            {
                throw new ArgumentException("The number of input elements is greater than the elements in the matrix.",
                    nameof(data));
            }

            RowSize = size;
            Data = new T[size * size];
            Array.Copy(data, Data, data.Length);
        }

        /// <summary>
        /// This constructor shows that derived classes can create an array with elements.
        /// </summary>
        protected SquareMatrix()
        {
        }
    }
}