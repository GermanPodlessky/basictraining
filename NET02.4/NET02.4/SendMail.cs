﻿using System;
using System.Net;
using System.Net.Mail;

namespace NET02._4
{
    /// <summary>
    /// The class for sending email to mail.
    /// </summary>
    public class SendMail
    {
        /// <summary>
        /// The method asynchronously sends an email to the mail.
        /// </summary>
        /// <param name="email">Email.</param>
        /// <param name="subject">Subject.</param>
        /// <param name="message">Message.</param>
        public static async void SendEmailAsync(string email, string subject, string message)
        {
            var mailAddress = new MailMessage("coherent2019@gmail.com", email) {Subject = subject, Body = message};

            var smtp = new SmtpClient("smtp.gmail.com", 587)
            {
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("coherent2019@gmail.com", "Coherent@2019")
            };
            await smtp.SendMailAsync(mailAddress);
            Console.WriteLine();
        }
    }
}