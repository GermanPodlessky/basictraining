﻿using System.Configuration;
using SiteConfig;

namespace NET02._4.Config
{
    /// <summary>
    /// The class to describe the sites section in the configuration file.
    /// </summary>
    class SiteSection : ConfigurationSection
    {
        [ConfigurationProperty("sites", IsRequired = true)]
        public SiteCollection Sites => (SiteCollection)this["sites"];
    }
}
