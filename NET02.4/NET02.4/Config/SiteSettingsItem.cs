﻿using System.Configuration;
using NET02._4;

namespace SiteConfig
{
    /// <summary>
    /// The class to describe the element of the sites.
    /// </summary>
    class SiteSettingsElement : ConfigurationElement, ISiteSettings
    {
        [ConfigurationProperty("interval", IsRequired = true)]
        public int Interval => (int) base["interval"];

        [ConfigurationProperty("timeOut", IsRequired = true)]
        public int TimeOut => (int) base["timeOut"];

        [ConfigurationProperty("url", IsRequired = true)]
        public string Url => (string) base["url"];

        [ConfigurationProperty("email", IsRequired = false)]
        public string Email => (string) base["email"];
    }
}