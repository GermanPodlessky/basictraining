﻿using System;
using System.Configuration;

namespace SiteConfig
{
    /// <summary>
    /// The class to describe the collection sites.
    /// </summary>
    [ConfigurationCollection(typeof(SiteSettingsElement))]
    class SiteCollection : ConfigurationElementCollection
    {
        public override ConfigurationElementCollectionType CollectionType =>
            ConfigurationElementCollectionType.BasicMap;

        public SiteSettingsElement this[int i] => (SiteSettingsElement) BaseGet(i);

        protected override string ElementName { get; } = "site";

        protected override ConfigurationElement CreateNewElement()
        {
            return new SiteSettingsElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as SiteSettingsElement)?.Url ?? throw new InvalidOperationException();
        }
    }
}