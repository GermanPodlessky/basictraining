﻿using System.Threading;

namespace NET02._4
{
    /// <summary>
    /// The class for describing the parameters of the request to the site.
    /// </summary>
    class SiteSettingsSettings : ISiteSettings
    {
        public int Interval { get; }
        public int TimeOut { get; }
        public string Url { get; }
        public string Email { get; }

        public SiteSettingsSettings(int interval, int timeOut, string url, string email)
        {
            Interval = interval;
            TimeOut = timeOut;
            Url = url;
            Email = email;
        }
    }
}