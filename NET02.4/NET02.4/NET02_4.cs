﻿using System;
using System.Configuration;
using System.Reflection;
using System.Threading;

namespace NET02._4
{
    class Net024
    {
        static void Main(string[] args)
        {
            using (var mutex = new Mutex(false, Assembly.GetExecutingAssembly().GetType().GUID.ToString()))
            {
                if (!mutex.WaitOne(TimeSpan.FromSeconds(1), false))
                {
                    return;
                }

                Console.WriteLine("Enter \"escape\" for exit");
                try
                {
                    var sitesMonitor = new SitesMonitor();
                    var configMonitor = new ConfigMonitor();
                    configMonitor.Loaded += sitesMonitor.Run;
                    configMonitor.Run();
                }
                catch (ConfigurationErrorsException)
                {
                    Console.WriteLine("The configuration file could not be read.");
                    return;
                }

                ConsoleKeyInfo key;
                do
                {
                    key = Console.ReadKey();
                } while (key.Key != ConsoleKey.Escape);
            }
        }
    }
}