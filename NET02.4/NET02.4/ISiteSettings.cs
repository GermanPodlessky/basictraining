﻿namespace NET02._4
{
    /// <summary>
    /// The interface for checked site description.
    /// </summary>
    public interface ISiteSettings
    {
        int TimeOut { get; }
        string Url { get; }
        string Email { get; }
    }
}