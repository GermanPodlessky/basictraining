﻿using System;
using System.Configuration;
using System.IO;
using NET02._4.Config;
using SiteConfig;

namespace NET02._4
{
    /// <summary>
    /// The class to load the configuration file when
    /// creating an instance and when changing the configuration file.
    /// </summary>
    class ConfigMonitor
    {
        private FileSystemWatcher _fsWatcher;
        private SiteCollection _sites;

        public event Action<SiteCollection> Loaded;

        /// <summary>
        /// The method starts monitoring changes in the configuration file.
        /// </summary>
        public void Run()
        {
            Load();
            _fsWatcher = new FileSystemWatcher
            {
                Path = AppDomain.CurrentDomain.SetupInformation.ApplicationBase,
                NotifyFilter = NotifyFilters.LastWrite,
                EnableRaisingEvents = true
            };
            _fsWatcher.Changed += OnChanged;
        }

        private void Load()
        {
            ConfigurationManager.RefreshSection("sitesSection");
            _sites = ((SiteSection) ConfigurationManager.GetSection("sitesSection")).Sites;
            Loaded?.Invoke(_sites);
        }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            _fsWatcher.EnableRaisingEvents = false;
            Load();
            _fsWatcher.EnableRaisingEvents = true;
        }
    }
}