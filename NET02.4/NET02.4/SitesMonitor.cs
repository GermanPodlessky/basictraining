﻿using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Threading;
using NLog;
using SiteConfig;

namespace NET02._4
{
    /// <summary>
    /// The class to track the response time of the site.
    /// </summary>
    class SitesMonitor
    {
        private static Logger _logger { get; }
        private List<Timer> _timers = new List<Timer>();


        static SitesMonitor()
        {
            var config = new NLog.Config.LoggingConfiguration();

            var logfile = new NLog.Targets.FileTarget("logfile") {FileName = "SiteMonitoringLog.txt"};
            config.AddRule(LogLevel.Debug, LogLevel.Fatal, logfile);

            LogManager.Configuration = config;
            _logger = LogManager.GetCurrentClassLogger();
        }

        /// <summary>
        /// The method starts monitoring sites.
        /// </summary>
        public void Run(SiteCollection sites)
        {
            TimerResourceReleases();
            foreach (var element in sites)
            {
                var site = (SiteSettingsElement) element;
                _timers.Add(new Timer(DetermineAvailabilitySite, site, 0, site.Interval));
            }
        }

        private void TimerResourceReleases()
        {
            _timers.ForEach(s => s.Dispose());
        }

        private static void DetermineAvailabilitySite(object data)
        {
            var site = (ISiteSettings) data;
            var timeOut = new Ping().Send(site.Url).RoundtripTime;
            if (timeOut > site.TimeOut)
            {
                SendMail.SendEmailAsync(site.Email, "Warning", site.Url + " not available.");
            }
            else
            {
                _logger.Info(site.Url + " available.");
            }
        }
    }
}