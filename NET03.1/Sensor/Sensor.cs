﻿using System;
using System.Runtime.Serialization;
using SensorLib.SerialMode;

namespace SensorLib
{
    /// <summary>
    /// Class for sensor description.
    /// </summary>
    [Serializable]
    [DataContract]
    public class Sensor
    {
        [DataMember] public Guid ID { get; private set; }
        [DataMember] public SensorType SensorType { get; set; }
        public SensorMode SensorMode { get; set; }

        public Sensor(IGenerateId generateId, SensorType sensorType)
        {
            ID = generateId.Generate();
            SensorType = sensorType;
            SensorMode = new SensorMode();
        }
    }
}