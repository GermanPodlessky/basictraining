﻿using System.Threading;

namespace SensorLib.SerialMode
{
    /// <summary>
    /// Abstract mode state class.
    /// </summary>
    public abstract class ModeState
    {
        /// <summary>
        /// The method to switch sensor mode.
        /// </summary>
        /// <param name="sensorMode">Sensor mode.</param>
        /// <param name="timer">Timer.</param>
        /// <param name="sensorFill">Sensor fill measurement.</param>
        public abstract void Switch(SensorMode sensorMode, Timer timer, SensorFillMeasurement sensorFill);
    }
}