﻿using System;
using System.Threading;

namespace SensorLib.SerialMode
{
    /// <summary>
    /// The class describing the state of idle.
    /// </summary>
    public class IdleState : ModeState
    {
        private static readonly Lazy<IdleState> _instance = new Lazy<IdleState>(() => new IdleState());

        public static IdleState GetInstance()
        {
            return _instance.Value;
        }

        public override void Switch(SensorMode sensorMode, Timer timer, SensorFillMeasurement sensorFill)
        {
            timer?.Dispose();
            sensorMode.State = CalibrationState.GetInstance();
        }

        public override string ToString()
        {
            return "IdleMode";
        }

        private IdleState()
        {
        }
    }
}