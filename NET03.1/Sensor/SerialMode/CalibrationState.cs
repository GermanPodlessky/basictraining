﻿using System;
using System.Threading;

namespace SensorLib.SerialMode
{
    /// <summary>
    /// The class describing the state of calibration.
    /// </summary>
    public class CalibrationState : ModeState
    {
        private static readonly Lazy<CalibrationState> _instance =
            new Lazy<CalibrationState>(() => new CalibrationState());

        // Milliseconds.
        public const int Interval = 1000;

        public static CalibrationState GetInstance()
        {
            return _instance.Value;
        }

        public override void Switch(SensorMode sensorMode, Timer timer, SensorFillMeasurement sensorFill)
        {
            sensorMode.State = WorkState.GetInstance();
            timer?.Dispose();
            timer = new Timer(CalibrationFill, sensorFill, 0, Interval);
        }

        public override string ToString()
        {
            return "CalibrationMode";
        }

        private CalibrationState()
        {
        }

        private void CalibrationFill(object data)
        {
            var sensor = (SensorFillMeasurement) data;
            if (sensor.MeasuredValue + 1 <= sensor.MaxValue &&
                sensor.MeasuredValue >= sensor.MinValue)
            {
                sensor.MeasuredValue++;
            }
        }
    }
}