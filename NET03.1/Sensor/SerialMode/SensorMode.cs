﻿using System;
using System.Threading;

namespace SensorLib.SerialMode
{
    /// <summary>
    /// The class describing sensor mode.
    /// </summary>
    public class SensorMode
    {
        private Timer _timer;

        public ModeState State = IdleState.GetInstance();

        /// <summary>
        /// The method to switch sensor mode.
        /// </summary>
        /// <param name="sensorFill">Sensor fill measurement.</param>
        public void Switch(SensorFillMeasurement sensorFill)
        {
            if (sensorFill == null)
            {
                throw new NullReferenceException();
            }

            State.Switch(this, _timer, sensorFill);
        }
    }
}