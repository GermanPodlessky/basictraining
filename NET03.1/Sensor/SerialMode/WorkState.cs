﻿using System;
using System.Threading;

namespace SensorLib.SerialMode
{
    /// <summary>
    /// The class describing the state of idle.
    /// </summary>
    public class WorkState : ModeState
    {
        private static readonly Lazy<WorkState> _instance = new Lazy<WorkState>(() => new WorkState());
        private readonly Random _random = new Random();

        public static WorkState GetInstance()
        {
            return _instance.Value;
        }

        public override void Switch(SensorMode sensorMode, Timer timer, SensorFillMeasurement sensorFill)
        {
            sensorMode.State = IdleState.GetInstance();
            timer?.Dispose();
            timer = new Timer(FillingAtWork, sensorFill, 0, sensorFill.MeasurementInterval);
        }

        public override string ToString()
        {
            return "WorkMode";
        }

        private WorkState()
        {
        }

        private void FillingAtWork(object data)
        {
            var sensor = (SensorFillMeasurement) data;
            sensor.MeasuredValue = _random.Next(sensor.MinValue, sensor.MaxValue);
        }
    }
}