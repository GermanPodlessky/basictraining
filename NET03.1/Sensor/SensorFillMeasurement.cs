﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using SensorLib.Observer;

namespace SensorLib
{
    /// <summary>
    /// Class for describing work with the sensor.
    /// </summary>
    [Serializable]
    [DataContract]
    public class SensorFillMeasurement
    {
        private double _measuredValue = double.NaN;
        private List<IObserver> _observers = new List<IObserver>();

        [DataMember] public int MinValue { get; set; }
        [DataMember] public int MaxValue { get; set; }
        [DataMember] public uint MeasurementInterval { get; set; }

        [XmlIgnore]
        public double MeasuredValue
        {
            get => _measuredValue;
            set
            {
                if (!(value <= MaxValue) || !(value >= MinValue))
                {
                    return;
                }

                _measuredValue = value;
                Notify();
            }
        }

        [DataMember] public Sensor Sensor { get; private set; }

        public SensorFillMeasurement(Sensor sensor, int minValue,
            int maxValue, uint measurementInterval)
        {
            Sensor = sensor;
            MeasurementInterval = measurementInterval;
            MinValue = minValue;
            MaxValue = maxValue;
        }

        /// <summary>
        /// The method to switch sensor mode.
        /// </summary>
        public void SwitchMode()
        {
            Sensor.SensorMode.Switch(this);
        }

        /// <summary>
        /// The method to add observers to the list of observers.
        /// </summary>
        /// <param name="observer">Observer.</param>
        public void Attach(IObserver observer)
        {
            if (_observers == null)
            {
                _observers = new List<IObserver>();
            }

            _observers.Add(observer);
        }

        /// <summary>
        /// The method to remove an observer from the observer list.
        /// </summary>
        /// <param name="observer"></param>
        public void Detach(IObserver observer)
        {
            if (_observers == null || !_observers.Exists(o => o == observer))
            {
                throw new ArgumentException("Argument not exits.", nameof(observer));
            }

            _observers.Remove(observer);
        }

        public override string ToString()
        {
            return Sensor.ID.ToString();
        }

        private void Notify()
        {
            foreach (var measurement in _observers)
            {
                measurement.Change(this);
            }
        }
    }
}