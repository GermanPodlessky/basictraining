﻿namespace SensorLib.Observer
{
    /// <summary>
    /// Monitoring interface
    /// </summary>
    public interface IObserver
    {
        void Change(SensorFillMeasurement sensorFill);
    }
}