﻿using System.Globalization;
using System.IO;

namespace SensorLib.Observer
{
    /// <summary>
    /// Supervisor class
    /// </summary>
    public class Observer : IObserver
    {
        /// <summary>
        /// The method is writing a change to a text file.
        /// </summary>
        /// <param name="sensorFill">Sensor fill measurement.</param>
        public void Change(SensorFillMeasurement sensorFill)
        {
            using (var fs = new FileStream("./Observer.txt", FileMode.OpenOrCreate))
            using (var sw = new StreamWriter(fs))
            {
                sw.WriteLine(sensorFill.MeasuredValue.ToString(CultureInfo.InvariantCulture));
            }
        }
    }
}