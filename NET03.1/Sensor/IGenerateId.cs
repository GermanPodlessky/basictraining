﻿using System;

namespace SensorLib
{
    /// <summary>
    /// The interface to create a unique identifier.
    /// </summary>
    public interface IGenerateId
    {
        /// <summary>
        /// The method generates a unique identifier.
        /// </summary>
        /// <returns>Unique identifier.</returns>
        Guid Generate();
    }
}