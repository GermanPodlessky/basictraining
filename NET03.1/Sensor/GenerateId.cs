﻿using System;

namespace SensorLib
{
    /// <summary>
    /// The class, an instance that is created once, to generate a unique identifier.
    /// </summary>
    public class GenerateId : IGenerateId
    {
        private static readonly Lazy<GenerateId> _instance = new Lazy<GenerateId>(() => new GenerateId());

        /// <summary>
        /// The method returns an object of the current class.
        /// </summary>
        /// <returns>Object of the current class.</returns>
        public static GenerateId GetInstance()
        {
            return _instance.Value;
        }

        public Guid Generate()
        {
            return Guid.NewGuid();
        }

        private GenerateId()
        {
        }
    }
}