﻿using System;

namespace SensorLib
{
    /// <summary>
    /// Sensor type.
    /// </summary>
    [Serializable]
    public enum SensorType
    {
        PressureSensor,
        TemperatureSensor,
        MagneticFieldSensor,
    }
}