﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;

namespace SensorLib.StorageOfSensorCharacteristics
{
    /// <summary>
    /// The class working with xml serialization.
    /// </summary>
    internal class XmlSensorSerializer : ISensorSerializer
    {
        private string _path;

        public string FileName { get; set; }

        public string Path
        {
            get => _path;
            set => _path = value ?? throw new ArgumentNullException(nameof(value));
        }

        public XmlSensorSerializer()
        {
        }

        public void Serialize(List<SensorFillMeasurement> sensors)
        {
            if (sensors == null)
            {
                throw new ArgumentNullException();
            }

            using (var fs = new FileStream(Path + FileName, FileMode.OpenOrCreate))
            {
                new DataContractSerializer(typeof(List<SensorFillMeasurement>)).WriteObject(fs, sensors);
            }
        }

        public List<SensorFillMeasurement> Deserialize()
        {
            if (!File.Exists(Path))
            {
                throw new FileNotFoundException("Xml file not found");
            }

            using (var fs = new FileStream(Path, FileMode.Open))
            {
                return (List<SensorFillMeasurement>)
                    new DataContractSerializer(typeof(List<SensorFillMeasurement>)).ReadObject(fs);
            }
        }
    }
}