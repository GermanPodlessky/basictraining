﻿using System;

namespace SensorLib.StorageOfSensorCharacteristics
{
    /// <summary>
    /// The class for creating json serializer.
    /// </summary>
    public class Factory
    {
        public static ISensorSerializer CreateSerializer(string type)
        {
            if (type.ToLower() == "xml")
            {
                return new XmlSensorSerializer();
            }

            return type.ToLower() == "json"
                ? new JsonSensorSerializer()
                : throw new ArgumentException();
        }
    }
}