﻿using System.Collections.Generic;

namespace SensorLib.StorageOfSensorCharacteristics
{
    /// <summary>
    /// The interface describing the serialization of sensors.
    /// </summary>
    public interface ISensorSerializer
    {
        string FileName { get; set; }
        string Path { get; set; }

        /// <summary>
        /// The method serializes the list of sensors to a file.
        /// </summary>
        /// <param name="sensors">List of sensors.</param>
        void Serialize(List<SensorFillMeasurement> sensors);

        /// <summary>
        /// The method deserializes the file.
        /// </summary>
        /// <returns>List of sensors.</returns>
        List<SensorFillMeasurement> Deserialize();
    }
}