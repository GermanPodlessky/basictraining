﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;

namespace SensorLib.StorageOfSensorCharacteristics
{
    /// <summary>
    /// The class working with json serialization.
    /// </summary>
    internal class JsonSensorSerializer : ISensorSerializer
    {
        private string _path;
        public string FileName { get; set; }

        public string Path
        {
            get => _path;
            set => _path = value ?? throw new ArgumentNullException(nameof(value));
        }

        public JsonSensorSerializer()
        {
        }

        public void Serialize(List<SensorFillMeasurement> sensors)
        {
            if (sensors == null)
            {
                throw new ArgumentNullException();
            }

            using (var fs = new FileStream(Path + FileName, FileMode.OpenOrCreate))
            {
                new DataContractJsonSerializer(typeof(List<SensorFillMeasurement>)).WriteObject(fs, sensors);
            }
        }

        public List<SensorFillMeasurement> Deserialize()
        {
            if (!File.Exists(Path))
            {
                throw new FileNotFoundException("Json file not found");
            }

            using (var fs = new FileStream(_path, FileMode.Open))
            {
                return (List<SensorFillMeasurement>) new DataContractJsonSerializer(typeof(List<SensorFillMeasurement>))
                    .ReadObject(fs);
            }
        }
    }
}