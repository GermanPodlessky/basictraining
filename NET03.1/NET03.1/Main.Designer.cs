﻿namespace NET03._1
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstSensors = new System.Windows.Forms.ListBox();
            this.lbl1 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lblSensorID = new System.Windows.Forms.Label();
            this.lbl3 = new System.Windows.Forms.Label();
            this.lblInterval = new System.Windows.Forms.Label();
            this.lbl4 = new System.Windows.Forms.Label();
            this.lblIntervalForWork = new System.Windows.Forms.Label();
            this.lbl5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdoJson = new System.Windows.Forms.RadioButton();
            this.rdoXml = new System.Windows.Forms.RadioButton();
            this.lblSensorMode = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblValue = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstSensors
            // 
            this.lstSensors.FormattingEnabled = true;
            this.lstSensors.Location = new System.Drawing.Point(26, 25);
            this.lstSensors.Name = "lstSensors";
            this.lstSensors.Size = new System.Drawing.Size(134, 316);
            this.lstSensors.TabIndex = 0;
            this.lstSensors.Click += new System.EventHandler(this.LstSensors_Click);
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Location = new System.Drawing.Point(23, 9);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(50, 13);
            this.lbl1.TabIndex = 1;
            this.lbl1.Text = "Датчики";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(26, 360);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(68, 23);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Добавить";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(100, 360);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(60, 23);
            this.btnDelete.TabIndex = 3;
            this.btnDelete.Text = "Удалить";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.Location = new System.Drawing.Point(254, 43);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(114, 13);
            this.lbl2.TabIndex = 5;
            this.lbl2.Text = "Id выбранного датча:";
            // 
            // lblSensorID
            // 
            this.lblSensorID.AutoSize = true;
            this.lblSensorID.Location = new System.Drawing.Point(375, 43);
            this.lblSensorID.Name = "lblSensorID";
            this.lblSensorID.Size = new System.Drawing.Size(0, 13);
            this.lblSensorID.TabIndex = 6;
            // 
            // lbl3
            // 
            this.lbl3.AutoSize = true;
            this.lbl3.Location = new System.Drawing.Point(254, 75);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(183, 13);
            this.lbl3.TabIndex = 7;
            this.lbl3.Text = "Интервал принимаемых значений:";
            // 
            // lblInterval
            // 
            this.lblInterval.AutoSize = true;
            this.lblInterval.Location = new System.Drawing.Point(443, 75);
            this.lblInterval.Name = "lblInterval";
            this.lblInterval.Size = new System.Drawing.Size(0, 13);
            this.lblInterval.TabIndex = 8;
            // 
            // lbl4
            // 
            this.lbl4.AutoSize = true;
            this.lbl4.Location = new System.Drawing.Point(254, 100);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(219, 13);
            this.lbl4.TabIndex = 9;
            this.lbl4.Text = "Интервал измерений в режиме \"Работа\":";
            // 
            // lblIntervalForWork
            // 
            this.lblIntervalForWork.AutoSize = true;
            this.lblIntervalForWork.Location = new System.Drawing.Point(479, 100);
            this.lblIntervalForWork.Name = "lblIntervalForWork";
            this.lblIntervalForWork.Size = new System.Drawing.Size(0, 13);
            this.lblIntervalForWork.TabIndex = 10;
            // 
            // lbl5
            // 
            this.lbl5.AutoSize = true;
            this.lbl5.Location = new System.Drawing.Point(254, 154);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(88, 13);
            this.lbl5.TabIndex = 11;
            this.lbl5.Text = "Режим датчика:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdoJson);
            this.groupBox1.Controls.Add(this.rdoXml);
            this.groupBox1.Location = new System.Drawing.Point(26, 389);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(164, 49);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "File for added.";
            // 
            // rdoJson
            // 
            this.rdoJson.AutoSize = true;
            this.rdoJson.Location = new System.Drawing.Point(74, 20);
            this.rdoJson.Name = "rdoJson";
            this.rdoJson.Size = new System.Drawing.Size(47, 17);
            this.rdoJson.TabIndex = 1;
            this.rdoJson.Text = "Json";
            this.rdoJson.UseVisualStyleBackColor = true;
            this.rdoJson.CheckedChanged += new System.EventHandler(this.RdoXml_CheckedChanged);
            // 
            // rdoXml
            // 
            this.rdoXml.AutoSize = true;
            this.rdoXml.Checked = true;
            this.rdoXml.Location = new System.Drawing.Point(7, 20);
            this.rdoXml.Name = "rdoXml";
            this.rdoXml.Size = new System.Drawing.Size(42, 17);
            this.rdoXml.TabIndex = 0;
            this.rdoXml.TabStop = true;
            this.rdoXml.Text = "Xml";
            this.rdoXml.UseVisualStyleBackColor = true;
            this.rdoXml.CheckedChanged += new System.EventHandler(this.RdoXml_CheckedChanged);
            // 
            // lblSensorMode
            // 
            this.lblSensorMode.AutoSize = true;
            this.lblSensorMode.Location = new System.Drawing.Point(332, 154);
            this.lblSensorMode.Name = "lblSensorMode";
            this.lblSensorMode.Size = new System.Drawing.Size(0, 13);
            this.lblSensorMode.TabIndex = 14;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(257, 170);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(110, 21);
            this.button1.TabIndex = 15;
            this.button1.Text = "Изменить режим";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.BtnSwitchMode);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(251, 126);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Измеренное значение:";
            // 
            // lblValue
            // 
            this.lblValue.AutoSize = true;
            this.lblValue.Location = new System.Drawing.Point(388, 126);
            this.lblValue.Name = "lblValue";
            this.lblValue.Size = new System.Drawing.Size(0, 13);
            this.lblValue.TabIndex = 17;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(651, 450);
            this.Controls.Add(this.lblValue);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblSensorMode);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lbl5);
            this.Controls.Add(this.lblIntervalForWork);
            this.Controls.Add(this.lbl4);
            this.Controls.Add(this.lblInterval);
            this.Controls.Add(this.lbl3);
            this.Controls.Add(this.lblSensorID);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.lstSensors);
            this.Name = "Main";
            this.Text = "Main";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstSensors;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label lblSensorID;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.Label lblInterval;
        private System.Windows.Forms.Label lbl4;
        private System.Windows.Forms.Label lblIntervalForWork;
        private System.Windows.Forms.Label lbl5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdoJson;
        private System.Windows.Forms.RadioButton rdoXml;
        private System.Windows.Forms.Label lblSensorMode;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblValue;
    }
}

