﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using SensorLib;
using SensorLib.Observer;
using SensorLib.SerialMode;
using SensorLib.StorageOfSensorCharacteristics;

namespace NET03._1
{
    public partial class Main : Form
    {
        private SensorFillMeasurement _item;
        private readonly OpenFileDialog _fileDialog = new OpenFileDialog {Filter = "(*.xml)|*.xml"};
        private readonly ISensorSerializer _jsonSerializer = Factory.CreateSerializer("json");
        private readonly ISensorSerializer _xmlSerializer = Factory.CreateSerializer("xml");

        public Main()
        {
            InitializeComponent();
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            if (_fileDialog.ShowDialog(this) != DialogResult.OK)
            {
                return;
            }

            if (rdoJson.Checked)
            {
                _jsonSerializer.Path = _fileDialog.FileName;
                _jsonSerializer.FileName = _fileDialog.SafeFileName;
                _jsonSerializer.Deserialize()
                    .ForEach(x =>
                    {
                        x.Sensor.SensorMode = new SensorMode();
                        x.Attach(new Observer());
                        lstSensors.Items.Add(x);
                    });
            }
            else
            {
                _xmlSerializer.Path = _fileDialog.FileName;
                _xmlSerializer.FileName = _fileDialog.SafeFileName;
                _xmlSerializer.Deserialize()
                    .ForEach(x =>
                    {
                        x.Sensor.SensorMode = new SensorMode();
                        x.Attach(new Observer());
                        lstSensors.Items.Add(x);
                    });
            }
        }

        private void LstSensors_Click(object sender, EventArgs e)
        {
            _item = (SensorFillMeasurement) lstSensors.SelectedItem;

            lblInterval.Text = $"[{_item.MinValue}; {_item.MaxValue}]";
            lblIntervalForWork.Text = _item.MeasurementInterval.ToString();
            lblSensorID.Text = _item.Sensor.ID.ToString();
            lblValue.Text = _item.MeasuredValue.ToString();
            lblSensorMode.Text = _item.Sensor.SensorMode.State.ToString();
        }

        private void RdoXml_CheckedChanged(object sender, EventArgs e)
        {
            var rdo = (RadioButton) sender;
            _fileDialog.Filter = $@"(*.{rdo.Text.ToLower()})|*.{rdo.Text.ToLower()}";
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            lstSensors.Items.Remove(_item);
            _item = null;
            SaveChanges();
        }

        private void SaveChanges()
        {
            if (rdoJson.Checked)
            {
                _jsonSerializer?.Serialize(
                    (List<SensorFillMeasurement>) lstSensors.Items.OfType<SensorFillMeasurement>());
            }
            else
            {
                _xmlSerializer?.Serialize(
                    (List<SensorFillMeasurement>) lstSensors.Items.OfType<SensorFillMeasurement>());
            }
        }

        private void BtnSwitchMode(object sender, EventArgs e)
        {
            _item?.SwitchMode();
            LstSensors_Click(null, null);
        }
    }
}