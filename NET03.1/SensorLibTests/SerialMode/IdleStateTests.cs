﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SensorLib.SerialMode;

namespace SensorLibTests.SerialMode
{
    [TestClass]
    public class IdleStateTests
    {
        [TestMethod]
        public void GetInstance()
        {
            Assert.IsTrue(IdleState.GetInstance() == IdleState.GetInstance());
        }
    }
}