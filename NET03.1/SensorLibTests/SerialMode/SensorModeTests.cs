﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SensorLib;
using SensorLib.SerialMode;

namespace SensorLibTests.SerialMode
{
    [TestClass]
    public class SensorModeTests
    {
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void SwitchNullParam()
        {
            new SensorMode().Switch(null);
        }

        [TestMethod]
        public void Switch()
        {
            // Arrange
            var sensorFill = new SensorFillMeasurement(new Sensor(GenerateId.GetInstance(), SensorType.PressureSensor),
                100, 1000, 10000);
            var sensorMode = new SensorMode();
            var notExpend = sensorFill.Sensor.SensorMode;


            // Act
            sensorMode.Switch(sensorFill);

            // Assert
            Assert.AreNotEqual(notExpend, sensorFill.Sensor.SensorMode.State);
        }
    }
}