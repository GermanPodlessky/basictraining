﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SensorLib;
using SensorLib.Observer;

namespace SensorLibTests
{
    [TestClass]
    public class SensorFillMeasurementTests
    {
        [TestMethod]
        public void AttachTest()
        {
            // Arrange
            var sensorFill = new SensorFillMeasurement(new Sensor(GenerateId.GetInstance(), SensorType.PressureSensor),
                100, 1000, 10000);
            var observer = new Observer();
            sensorFill.Attach(observer);

            // Act
            sensorFill.Detach(observer); 

        }

        [TestMethod]
        public void SwitchMode()
        {
            // Arrange
            var sensorFill = new SensorFillMeasurement(new Sensor(GenerateId.GetInstance(), SensorType.PressureSensor),
                100, 1000, 10000);
            var notExpend = sensorFill.Sensor.SensorMode;


            // Act
            sensorFill.SwitchMode();

            // Assert
            Assert.AreNotEqual(notExpend, sensorFill.Sensor.SensorMode.State);
        }
    }
}