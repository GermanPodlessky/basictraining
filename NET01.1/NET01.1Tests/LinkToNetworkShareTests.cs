﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NET01._1.Tests
{
    [TestClass]
    public class LinkToNetworkShareTests
    {
        [TestMethod]
        public void SetValidlStringContentUri()
        {
            // Arrange
            var linkToNetworkShare = new LinkToNetworkShare("Test");
            var expected = "Test";

            // Act
            var actual = linkToNetworkShare.ContentUri;

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SetEmptyStringContentUri()
        {
            var linkToNetworkShare = new LinkToNetworkShare("Test");
            linkToNetworkShare.ContentUri = string.Empty;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SetNullContentUri()
        {
            var linkToNetworkShare = new LinkToNetworkShare("Test");
            linkToNetworkShare.ContentUri = null;
        }

        [TestMethod]
        public void CloneChangeThePropOfOriginal()
        {
            // Arrange
            var linkToNetworkShare = new LinkToNetworkShare("Test");
            var expexted = linkToNetworkShare.ContentUri;

            // Act
            var cloneLinkToNetworkShare = (LinkToNetworkShare) linkToNetworkShare.Clone();
            linkToNetworkShare.ContentUri = "test1";

            var actual = cloneLinkToNetworkShare.ContentUri;

            // Assert
            Assert.AreEqual(expexted, actual);
        }
    }
}