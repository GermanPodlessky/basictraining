﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NET01._1.Tests
{
    [TestClass]
    public class EntityTests
    {
        [TestMethod]
        public void SetValidStringDescription()
        {
            // Arrange
            var textMaterial = new TextMaterial("Test");
            var expected = "Test2";

            // Act
            textMaterial.Description = "Test2";
            var actual = textMaterial.Description;

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void SetNullDescription()
        {
            // Arrange
            var textMaterial = new TextMaterial("Test");

            // Act
            textMaterial.Description = null;
            var actual = textMaterial.Description;

            // Assert
            Assert.IsNull(actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SetNotValidlStringDescription()
        {
            var textMaterial = new TextMaterial("Test");
            textMaterial.Description = new string(' ', 257);
        }

        [TestMethod]
        public void ToString()
        {
            // Arrange
            var textMaterial = new TextMaterial("Test") {Description = "Test"};
            var expected = "Test";

            // Act
            var actual = textMaterial.ToString();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Equals()
        {
            // Arrange
            var textMaterial = (Entity) new TextMaterial("Test");
            var temp = textMaterial;

            // Act
            var actual = textMaterial.Equals(temp);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void GetHashCode()
        {
            // Arrange
            var textMaterial = new TextMaterial("Test");
            var temp = textMaterial;
            var expected = textMaterial.GetHashCode();

            // Act
            var actual = temp.GetHashCode();

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}