﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NET01._1.Tests
{
    [TestClass]
    public class TextMaterialTests
    {
        public void SetValidlStringText()
        {
            // Arrange
            var textMaterial = new TextMaterial("Test");
            var expected = "Test";

            // Act
            var actual = textMaterial.Text;

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SetEmptyStringContentUri()
        {
            var textMaterial = new TextMaterial("Test");
            textMaterial.Text = string.Empty;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SetNullContentUri()
        {
            var textMaterial = new TextMaterial("Test");
            textMaterial.Text = null;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SetStringLengthMoreThanMaxLengthTextContentUri()
        {
            var textMaterial = new TextMaterial("Test");
            textMaterial.Text = new string(' ', 10001);
        }

        [TestMethod]
        public void CloneChangeThePropOfOriginal()
        {
            // Arrange
            var textMaterial = new TextMaterial("Test");
            var expexted = textMaterial.Text;

            // Act
            var cloneLinkToNetworkShare = (TextMaterial) textMaterial.Clone();
            textMaterial.Text = "test1";

            var actual = cloneLinkToNetworkShare.Text;

            // Assert
            Assert.AreEqual(expexted, actual);
        }
    }
}