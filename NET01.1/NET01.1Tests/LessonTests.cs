﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NET01._1.Tests
{
    [TestClass]
    public class LessonTests
    {
        [TestMethod]
        public void AddMaterials()
        {
            // Arrange
            var textMaterial = new TextMaterial("Test");
            var linkToNetworkShare = new LinkToNetworkShare("Test");
            var lesson = new Lesson {textMaterial, linkToNetworkShare};

            // Act
            var actual = lesson.GetMaterials()[0].Equals(textMaterial) &&
                         lesson.GetMaterials()[1].Equals(linkToNetworkShare);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void AddNull()
        {
            // Arrange
            var lesson = new Lesson {new TextMaterial("Test"), new LinkToNetworkShare("Test")};
            var expected = lesson.GetMaterials().Count;

            // Act
            lesson.Add(null);
            var actual = lesson.GetMaterials().Count;

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetTextTypeOfLesson()
        {
            // Arrange
            var lesson = new Lesson {new TextMaterial("Test")};
            var expected = LessonType.TextLesson;

            // Act
            var actual = lesson.GetTypeOfLesson();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetVideoTypeOfLesson()
        {
            // Arrange
            var lesson = new Lesson {new TextMaterial("Test"), new VideoMaterial("Test")};
            var expected = LessonType.VideoLesson;

            // Act
            var actual = lesson.GetTypeOfLesson();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void CloneRemoveLinksFromTheOriginal()
        {
            // Arrange
            var lesson = new Lesson {new TextMaterial("Test"), new LinkToNetworkShare("Test")};

            // Act
            var cloneLessonn = (Lesson) lesson.Clone();
            lesson.GetMaterials()[0] = null;
            var actual = cloneLessonn.GetMaterials()[0];

            // Assert
            Assert.IsNotNull(actual);
        }
    }
}