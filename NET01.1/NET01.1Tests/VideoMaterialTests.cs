﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NET01._1.Tests
{
    [TestClass]
    public class VideoMaterialTests
    {
        [TestMethod]
        public void SetValidlStringContentUri()
        {
            // Arrange
            var videoMaterial = new VideoMaterial("Test");
            var expected = "Test";

            // Act
            var actual = videoMaterial.ContentUri;

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SetEmptyStringContentUri()
        {
            var videoMaterial = new VideoMaterial("Test");
            videoMaterial.ContentUri = string.Empty;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SetNullContentUri()
        {
            var videoMaterial = new VideoMaterial("Test");
            videoMaterial.ContentUri = null;
        }

        [TestMethod]
        public void CloneChangeThePropOfOriginal()
        {
            // Arrange
            var videoMaterial = new VideoMaterial("Test");
            var expexted = videoMaterial.ContentUri;

            // Act
            var cloneVideoMaterial = (VideoMaterial) videoMaterial.Clone();
            videoMaterial.ContentUri = "test1";

            var actual = cloneVideoMaterial.ContentUri;

            // Assert
            Assert.AreEqual(expexted, actual);
        }
    }
}