﻿namespace NET01._1
{
    public interface IGenerateVersion
    {
        /// <summary>
        /// The method generates a version.
        /// </summary>
        /// <param name="sizeVersion">Version array size.</param>
        /// <returns></returns>
        byte[] Generate(int sizeVersion);
    }
}
