﻿namespace NET01._1
{
    /// <summary>
    /// Type of occupation.
    /// </summary>
    public enum LessonType
    {
        VideoLesson,
        TextLesson,
    }
}