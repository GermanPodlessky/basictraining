﻿using System;

namespace NET01._1
{
    /// <summary>
    /// Description of the text material.
    /// </summary>
    public class TextMaterial : Entity
    {
        private const int TextMaxlength = 10000;
        private string _text;

        public string Text
        {
            get => _text;
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(Text));
                }

                if (value == string.Empty)
                {
                    throw new ArgumentException($"{nameof(Text)} must not be empty");
                }

                if (value.Length > TextMaxlength)
                {
                    throw new ArgumentException($"{nameof(Text)} length can not exceed {TextMaxlength}");
                }

                _text = value;
            }
        }

        public TextMaterial(string text)
        {
            Text = text;
        }

        protected TextMaterial(string id, string description)
            : base(id, description)
        {
        }

        public override object Clone()
        {
            return new TextMaterial(Id, Description) {Text = Text};
        }
    }
}