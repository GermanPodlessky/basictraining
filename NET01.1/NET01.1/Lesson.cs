﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace NET01._1
{
    /// <summary>
    /// The class description of the lesson.
    /// </summary>
    public class Lesson : Entity, IVersionable, IEnumerable
    {
        private List<Entity> _materials = new List<Entity>();
        private const int VersionSize = 8;
        private byte[] _version = new byte[VersionSize];

        public Lesson()
        {
        }

        public Lesson(IGenerateVersion generate)
        {
            SetVersion(generate);
        }

        protected Lesson(string id, string description)
            : base(id, description)
        {
        }


        /// <summary>
        /// The method gets materials.
        /// </summary>
        /// <returns></returns>
        public List<Entity> GetMaterials()
        {
            return _materials;
        }

        /// <summary>
        /// The method adding material to the array of materials.
        /// </summary>
        /// <param name="material">Object to add.</param>
        public void Add(Entity material)
        {
            if (material == null)
            {
                throw new ArgumentNullException(nameof(material));
            }

            _materials.Add(material);
        }

        /// <summary>
        /// The method getting an Activity Type.
        /// </summary>
        /// <returns>Type of occupation.</returns>
        public LessonType GetTypeOfLesson()
        {
            foreach (var t in _materials)
            {
                if (t is VideoMaterial)
                {
                    return LessonType.VideoLesson;
                }
            }

            return LessonType.TextLesson;
        }

        public byte[] GetVersion()
        {
            return _version;
        }

        public void SetVersion(IGenerateVersion generate)
        {
            _version = generate.Generate(VersionSize);
        }


        public override object Clone()
        {
            return new Lesson(Id, Description)
            {
                _version = _version,
                _materials = _materials
            };
        }

        public IEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}