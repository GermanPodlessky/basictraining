﻿using System;

namespace NET01._1
{
    /// <summary>
    /// Description of the material with a link to a network resource.
    /// </summary>
    public class LinkToNetworkShare : Entity
    {
        private string _contentUri;

        public string ContentUri
        {
            get => _contentUri;
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(ContentUri));
                }

                if (value == string.Empty)
                {
                    throw new ArgumentException("Text must not be empty");
                }

                _contentUri = value;
            }
        }

        public LinkType LinkType { get; set; }

        public LinkToNetworkShare(string contentUri)
        {
            ContentUri = contentUri;
        }

        private LinkToNetworkShare(string id, string description)
            : base(id, description)
        {
        }


        public override object Clone()
        {
            return new LinkToNetworkShare(Id, Description)
            {
                ContentUri = ContentUri,
                LinkType = LinkType,
            };
        }
    }
}