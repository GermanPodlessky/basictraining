﻿namespace NET01._1
{
    public enum VideoFormat
    {
        Unknown,
        Avi,
        Mp4,
        Flv,
    }
}