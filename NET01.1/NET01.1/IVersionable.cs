﻿namespace NET01._1
{
    /// <summary>
    /// The interface defines a set of methods for the version.
    /// </summary>
    public interface IVersionable
    {
        /// <summary>
        /// The method get object version.
        /// </summary>
        /// <returns>An array storing the version of the object.</returns>
        byte[] GetVersion();

        /// <summary>
        /// The method generation and set version.
        /// </summary>
        void SetVersion(IGenerateVersion generate);
    }
}