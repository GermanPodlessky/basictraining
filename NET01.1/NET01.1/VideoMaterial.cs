﻿using System;

namespace NET01._1
{
    /// <summary>
    /// Description of the video material.
    /// </summary>
    public class VideoMaterial : Entity, IVersionable
    {
        private string _contentUri;
        private const int SizeVersion = 8;
        private byte[] _version = new byte[SizeVersion];

        public string ContentUri
        {
            get => _contentUri;
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(ContentUri));
                }

                if (value == string.Empty)
                {
                    throw new ArgumentException("Text must not be empty");
                }

                _contentUri = value;
            }
        }

        public string PictureSaverUri { get; set; }

        public VideoFormat VideoFormat { get; set; }

        public VideoMaterial(string contentUri)
        {
            ContentUri = contentUri;
        }

        public VideoMaterial(string contentUri, IGenerateVersion generate)
            : this(contentUri)
        {
            SetVersion(generate);
        }

        protected VideoMaterial(string id, string description)
            : base(id, description)
        {
        }

        public byte[] GetVersion()
        {
            return _version;
        }

        public void SetVersion(IGenerateVersion generate)
        {
            _version = generate.Generate(SizeVersion);
        }

        public override object Clone()
        {
            return new VideoMaterial(Id, Description)
            {
                _version = _version,
                ContentUri = ContentUri,
                PictureSaverUri = PictureSaverUri,
                VideoFormat = VideoFormat
            };
        }
    }
}