﻿using System;

namespace NET01._1
{
    /// <summary>
    /// The base class for describing the essence of the lesson and lesson materials.
    /// </summary>
    public abstract class Entity : ICloneable
    {
        private const int DescriptionMaxLength = 256;

        private string _description;

        public string Id { get; private set; }

        public string Description
        {
            get => _description;
            set
            {
                if (value?.Length > DescriptionMaxLength)
                {
                    throw new ArgumentException($"{nameof(Description)} length can not exceed {DescriptionMaxLength}");
                }

                _description = value;
            }
        }

        protected Entity()
        {
            Id = StringExtension.GenerateNewId(default);
        }

        protected Entity(string id, string description)
        {
            Id = id;
            Description = description;
        }

        /// <summary>
        /// The method returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return Description;
        }

        /// <summary>
        /// The method Checks the equality of two objects (the current with the transmitted).
        /// </summary>
        /// <param name="obj">Object with which compare.</param>
        /// <returns>Boolean equality of the current object with the passed.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (this == obj)
            {
                return true;
            }

            return Id == ((Entity)obj).Id;
        }

        /// <summary>
        /// The method finds the hash code of the current object.
        /// </summary>
        /// <returns>Object hash code.</returns>
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// The method creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of current instance.</returns>
        public abstract object Clone();
    }
}