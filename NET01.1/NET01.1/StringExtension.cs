﻿using System;

namespace NET01._1
{
    /// <summary>
    /// String class extension.
    /// </summary>
    public static class StringExtension
    {
        ///// <summary>
        ///// The method setting a new ID.
        ///// </summary>
        ///// <param name="str">Entity for which a new one is installed id.</param>
        public static string GenerateNewId(this string str)
        {
            return Guid.NewGuid().ToString();
        }
    }
}