﻿using System;
using System.Linq;

namespace NET01._1
{
    /// <summary>
    /// The class for describing the generator version.
    /// </summary>
    public class GenerateVersion : IGenerateVersion
    {
        public byte[] Generate(int sizeVersion)
        {
            return Enumerable.Range(0, sizeVersion).Select(s => (byte) new Random(s).Next(byte.MaxValue)).ToArray();
        }
    }
}