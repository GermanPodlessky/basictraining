﻿using System;

namespace NET02._1
{
    /// <summary>
    /// The class for the description of the author.
    /// </summary>
    public class Author
    {
        private const int Maxlength = 200;
        private string _firstName;
        private string _lastName;

        /// <summary>
        /// Sets the correct first name.
        /// </summary>
        public string FirstName
        {
            get => _firstName;
            set
            {
                CheckName(value);
                _firstName = value;
            }
        }

        /// <summary>
        /// Sets the correct last name.
        /// </summary>
        public string LastName
        {
            get => _lastName;
            set
            {
                CheckName(value);
                _lastName = value;
            }
        }

        public Author(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        private void CheckName(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentException($"{nameof(LastName)} must not be empty or null");
            }

            if (value.Length > Maxlength)
            {
                throw new ArgumentException($"{nameof(LastName)} length can not exceed {Maxlength}");
            }
        }
    }
}