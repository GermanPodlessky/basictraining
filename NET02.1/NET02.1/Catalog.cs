﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NET02._1
{
    /// <summary>
    /// The class for describing the catalog of books.
    /// </summary>
    public class Catalog : IEnumerable<Book>
    {
        private readonly List<Book> _books = new List<Book>();

        public Book this[string isbn] => _books.SingleOrDefault(b => b.Isbn == isbn.Replace("-", ""));

        public void Add(Book book)
        {
            if (book == null)
            {
                throw new ArgumentNullException(nameof(book));
            }

            if (_books.Any(b => b.Equals(book)))
            {
                throw new ArgumentException();
            }

            _books.Add(book);
        }

        public IEnumerable<Book> GetBooksByAuthor(Author author)
        {
            return _books.Where(b => b.Authors.Exists(a =>
                string.Equals(a.FirstName, author.FirstName, StringComparison.CurrentCultureIgnoreCase) &&
                string.Equals(a.LastName, author.LastName, StringComparison.CurrentCultureIgnoreCase)));
        }

        public IEnumerable<Book> GetBooksFromNewToOld()
        {
            return _books.OrderByDescending(b => b.PublicationDate.Date);
        }

        public IEnumerable<(Author, int)> GetPairsAuthorNumberOfBooks()
        {
            return _books.SelectMany(x => x.Authors)
                .GroupBy(a => new
                    {FirstName = a.FirstName.ToLower(), LastName = a.LastName.ToLower()})
                .Select(x => (new Author(x.Key.FirstName, x.Key.LastName), x.Count()));
        }

        public IEnumerator<Book> GetEnumerator()
        {
            return _books.OrderBy(b => b.Name).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}