﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace NET02._1
{
    /// <summary>
    /// The class to describe the book.
    /// </summary>
    public class Book
    {
        private const int NameMaxlength = 1000;
        private static readonly Regex _pattern = new Regex("^([0-9]{3}-[0-9]-[0-9]{2}-[0-9]{6}-[0-9]|[0-9]{13})$");
        private string _isbn;
        private string _name;
        private List<Author> _authors = new List<Author>();
        private DateTime _date;

        /// <summary>
        /// Sets the correct isbn.
        /// </summary>
        public string Isbn
        {
            get => _isbn;
            set
            {
                if (_pattern.IsMatch(value))
                {
                    _isbn = value.Replace("-", "");
                }
                else
                {
                    throw new ArgumentException("Invalid isbn");
                }
            }
        }

        /// <summary>
        /// Sets the correct name.
        /// </summary>
        public string Name
        {
            get => _name;
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException($"{nameof(Name)} must not be empty or null");
                }

                if (value.Length > NameMaxlength)
                {
                    throw new ArgumentException($"{nameof(Name)} length can not exceed {NameMaxlength}");
                }

                _name = value;
            }
        }

        /// <summary>
        /// Sets the correct date.
        /// </summary>
        public DateTime PublicationDate
        {
            get => _date;
            set
            {
                if (value > DateTime.Now.Date)
                {
                    throw new ArgumentException("This day has not yet been.");
                }

                _date = value.Date;
            }
        }

        /// <summary>
        /// Sets the correct authors collection.
        /// </summary>
        public List<Author> Authors
        {
            get => _authors;
            set => _authors = value ?? throw new ArgumentNullException(nameof(Authors));
        }

        public Book(string name, string isbn)
        {
            Name = name;
            Isbn = isbn;
        }

        /// <summary>
        /// The method Checks the equality of two objects (the current with the transmitted).
        /// </summary>
        /// <param name="obj">Object with which compare.</param>
        /// <returns>Boolean equality of the current object with the passed.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (this == obj)
            {
                return true;
            }

            if (obj.GetType() != GetType())
            {
                return false;
            }

            var other = (Book) obj;
            return Isbn.Replace("-", "") == other.Isbn.Replace("-", "");
        }

        /// <summary>
        /// The method finds the hash code of the current object.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return Isbn.GetHashCode();
        }
    }
}