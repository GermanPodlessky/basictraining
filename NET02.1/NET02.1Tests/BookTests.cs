﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NET02._1.Tests
{
    [TestClass]
    public class BookTests
    {
        [TestMethod]
        public void SetValidStringName()
        {
            // Arrange
            var book = new Book("Test", "000-0-00-000000-2");
            var expected = "Test";

            // Act
            var actual = book.Name;

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SetNullName()
        {
            new Book("Test", "000-0-00-000000-2") {Name = null};
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SetEmptyStringContentUri()
        {
            new Book("Test", "000-0-00-000000-2") {Name = string.Empty};
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SetStringLengthMoreThanMaxLengthTextContentUri()
        {
            new Book("Test", "000-0-00-000000-2") {Name = new string(' ', 1001)};
        }

        [TestMethod]
        public void SetValidStringIsbnPatternOne()
        {
            // Arrange
            var book = new Book("Test", "000-0-00-000000-2");
            var expected = "0000000000002";

            // Act
            var actual = book.Isbn;

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void SetValidStringIsbnPatternSecond()
        {
            // Arrange
            var book = new Book("Test", "0000000000002");
            var expected = "0000000000002";

            // Act
            var actual = book.Isbn;

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SetIncorrectStringIsbn()
        {
            new Book("Test", "0000-000000002");
        }

        [TestMethod]
        public void Equals()
        {
            // Arrange
            var bookA = new Book("Test", "0000000000002");
            var bookB = new Book("Test", "0000000000002");

            // Act
            var condition = bookA.Equals(bookB);

            // Assert
            Assert.IsTrue(condition);
        }

        [TestMethod]
        public void GetHashCode()
        {
            // Arrange
            var bookA = new Book("Test", "0000000000002");
            var bookB = new Book("Test", "0000000000002");
            var expected = bookA.GetHashCode();

            // Act
            var actual = bookB.GetHashCode();

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}