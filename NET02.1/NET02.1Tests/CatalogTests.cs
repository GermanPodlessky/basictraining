﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NET02._1.Tests
{
    [TestClass]
    public class CatalogTests
    {
        [TestMethod]
        public void AddValidBook()
        {
            // Arrange
            var catalog = new Catalog {new Book("Test", "000-0-00-000000-2")};

            // Act
            var value = catalog["000-0-00-000000-2"];

            // Assert
            Assert.IsNotNull(value);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddNullBook()
        {
            new Catalog {null};
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void AddExistBook()
        {
            var catalog = new Catalog {new Book("Test", "000-0-00-000000-2")};
            catalog.Add(new Book("Test1", "000-0-00-000000-2"));
        }

        [TestMethod]
        public void GetBooksByAuthor()
        {
            // Arrange
            var author1 = new Author("Author1", "Author1");
            var author2 = new Author("Author2", "Author2");
            var catalog = new Catalog
            {
                new Book("Test", "000-0-00-000000-1")
                {
                    Authors = new List<Author>
                    {
                        author2, author1
                    }
                },
                new Book("Test2", "000-0-00-000000-2")
                {
                    Authors = new List<Author> {author1}
                }
            };

            // Act
            var enumerator = catalog.GetBooksByAuthor(author1).GetEnumerator();
            while (enumerator.MoveNext())
            {
                Assert.AreEqual(enumerator.Current?.Isbn, "0000000000001");
                enumerator.MoveNext();
                Assert.AreEqual(enumerator.Current?.Isbn, "0000000000002");
            }

            enumerator = catalog.GetBooksByAuthor(author2).GetEnumerator();
            while (enumerator.MoveNext())
            {
                Assert.AreEqual(enumerator.Current?.Isbn, "0000000000001");
            }
        }

        [TestMethod]
        public void GetBooksFromNewToOld()
        {
            // Arrange
            var book1 = new Book("Test", "0000000000001")
            {
                Authors = new List<Author>
                {
                    new Author("Author2", "Author2"), new Author("Author1", "Author1")
                },
                PublicationDate = new DateTime(2019, 1, 1)
            };
            var book2 = new Book("Test2", "0000000000002")
            {
                Authors = new List<Author> {new Author("Author1", "Author1")},
                PublicationDate = new DateTime(2019, 3, 3)
            };
            var catalog = new Catalog {book1, book2};

            // Act
            var enumerator = catalog.GetBooksFromNewToOld().GetEnumerator();
            while (enumerator.MoveNext())
            {
                Assert.AreEqual(enumerator.Current, book2);
                enumerator.MoveNext();
                Assert.AreEqual(enumerator.Current, book1);
            }
        }

        [TestMethod]
        public void GetPairsAuthorNumberOfBooks()
        {
            // Arrange
            var author1 = new Author("Author1", "Author1");
            var author2 = new Author("Author2", "Author2");
            var book1 = new Book("Test", "000-0-00-000000-1")
            {
                Authors = new List<Author>
                {
                    author2, author1
                }
            };
            var book2 = new Book("Test2", "000-0-00-000000-2")
            {
                Authors = new List<Author> {author1}
            };
            var catalog = new Catalog {book1, book2};

            // Act
            var enumerator = catalog.GetPairsAuthorNumberOfBooks().GetEnumerator();
            while (enumerator.MoveNext())
            {
                Assert.AreEqual(enumerator.Current.Item1.FirstName, author2.FirstName.ToLower());
                Assert.AreEqual(enumerator.Current.Item2, 1);
                enumerator.MoveNext();
                Assert.AreEqual(enumerator.Current.Item1.FirstName, author1.FirstName.ToLower());
                Assert.AreEqual(enumerator.Current.Item2, 2);
            }
        }

        [TestMethod]
        public void GetEnumerator()
        {
            // Arrange
            var book1 = new Book("AaB", "000-0-00-000000-1")
            {
                Authors = new List<Author>
                {
                    new Author("Author2", "Author2"), new Author("Author1", "Author1")
                }
            };
            var book2 = new Book("AAA", "000-0-00-000000-2")
            {
                Authors = new List<Author> {new Author("Author1", "Author1")}
            };
            var catalog = new Catalog {book1, book2};

            // Act
            var enumerator = catalog.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Assert.AreEqual(enumerator.Current, book2);
                enumerator.MoveNext();
                Assert.AreEqual(enumerator.Current, book1);
            }
        }
    }
}