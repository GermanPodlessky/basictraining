﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace NET02._1.Tests
{
    [TestClass]
    public class AuthorTests
    {
        [TestMethod]
        public void SetValidStringName()
        {
            // Arrange
            var author = new Author("Test", "Test");
            var expected = "Test";

            // Act
            var actual = author.FirstName;

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SetNullName()
        {
            new Author("Test", "Test") {FirstName = null};
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SetEmptyStringContentUri()
        {
            new Author("Test", "Test") {FirstName = string.Empty};
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SetStringLengthMoreThanMaxLengthTextContentUri()
        {
            new Author("Test", "Test") {FirstName = new string(' ', 201)};
        }
    }
}