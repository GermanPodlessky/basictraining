﻿using System;

namespace TrackingAttribute
{
    /// <summary>
    /// Attribute to get field and property name.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class TrackingPropertyAttribute : Attribute
    {
        public string Name { get; }

        public TrackingPropertyAttribute(string name)
        {
            Name = name;
        }
    }
}