﻿using System;

namespace TrackingAttribute
{
    /// <summary>
    /// Attribute to get type name.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    public class TrackingEntityAttribute : Attribute
    {
        public string Name { get; }

        public TrackingEntityAttribute(string name)
        {
            Name = name;
        }
    }
}