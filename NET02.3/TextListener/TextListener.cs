﻿using System;
using System.Collections.Generic;
using System.IO;
using Listener;

namespace TextListener
{
    /// <summary>
    /// The class for describing the event log listener to a text file.
    /// </summary>
    public class TextListener : IListener
    {
        private readonly string _fileName;

        public TextListener(Dictionary<string, string> config)
        {
            if (config == null)
            {
                throw new ArgumentNullException();
            }

            _fileName = config.ContainsKey("fileName") ? config["fileName"] : throw new KeyNotFoundException();
        }

        /// <summary>
        /// The method for writing logs to a text file.
        /// </summary>
        /// <param name="logLevel">Minimum logging level.</param>
        /// <param name="message">Message for log.</param>
        public void Write(LogLevel logLevel, string message)
        {
            using (var writer = new StreamWriter(_fileName, true))
            {
                writer.WriteLineAsync($"{DateTime.Now} | {logLevel} | {message}");
            }
        }
    }
}