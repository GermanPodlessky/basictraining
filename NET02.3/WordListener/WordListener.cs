﻿using System;
using System.Collections.Generic;
using Listener;
using Word = Microsoft.Office.Interop.Word;

namespace WordListener
{
    /// <summary>
    /// The class for describing the event log listener to a word file.
    /// </summary>
    public class WordListener : IListener
    {
        private readonly string _fileName;
        private readonly Word.Application _app = new Word.Application();
        private Word.Document _document;

        public WordListener(Dictionary<string, string> config)
        {
            if (config == null)
            {
                throw new ArgumentNullException();
            }

            _fileName = config.ContainsKey("fileName") ? config["fileName"] : throw new KeyNotFoundException();
            OpenOrCreateDocFile();
        }

        /// <summary>
        /// The method for writing logs to a word file.
        /// </summary>
        /// <param name="logLevel">Minimum logging level.</param>
        /// <param name="message">Message for log.</param>
        public void Write(LogLevel logLevel, string message)
        {
            _document = _app.Documents.Open(_fileName, Visible: false);
            _document.Content.Text += $"{DateTime.Now} | {logLevel} | {message}";
            _document.Save();
            _document.Close();
        }

        private void OpenOrCreateDocFile()
        {
            try
            {
                _app.Documents.Open(_fileName, Visible: false);
            }
            catch
            {
                _document = _app.Documents.Add();
                _document.SaveAs(_fileName);
                _document.Close();
            }
        }
    }
}