﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Listener;

namespace EventLogListener
{
    /// <summary>
    /// The class for describing the Windows event log listener.
    /// </summary>
    public class EventLogListener : IListener
    {
        private readonly string _logName;
        private readonly EventLogEntryType _type;

        public EventLogListener(Dictionary<string, string> config)
        {
            if (config == null)
            {
                throw new ArgumentNullException();
            }

            _logName = config.ContainsKey("logName") ? config["logName"] : "Application";
            if (!Enum.TryParse(config["logType"], true, out _type))
            {
                _type = EventLogEntryType.Information;
            }
        }

        /// <summary>
        /// The method for writing logs in the Windows event log.
        /// </summary>
        /// <param name="logLevel">Minimum logging level.</param>
        /// <param name="message">Message for log.</param>
        public void Write(LogLevel logLevel, string message)
        {
            using (var eventLog = new EventLog(_logName))
            {
                eventLog.Source = _logName;
                eventLog.WriteEntry(message, _type, 101, 1);
            }
        }
    }
}