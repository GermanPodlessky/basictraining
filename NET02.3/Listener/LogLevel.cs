﻿namespace Listener
{
    /// <summary>
    /// Log level
    /// </summary>
    public enum LogLevel
    {
        Trace,
        Debug,
        Info,
        Warn,
        Error,
        Fatal
    }
}