﻿namespace Listener
{
    /// <summary>
    /// The listener interface with the write method.
    /// </summary>
    public interface IListener
    {
        void Write(LogLevel logLevel, string message);
    }
}