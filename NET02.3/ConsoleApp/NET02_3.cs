﻿using Listener;
using TrackingAttribute;


namespace ConsoleApp
{
    class NET02_3
    {
        static void Main(string[] args)
        {
            var logger = new Logger.Logger();
            logger.Write(LogLevel.Fatal, "Test");

            logger.Track(new Employee {Name = "German", MonthExperience = 1});
        }

        /// <summary>
        /// The class for testing.
        /// </summary>
        [TrackingEntity(nameof(Employee))]
        private class Employee
        {
            [TrackingProperty(nameof(Name))]
            public string Name { get; set; }

            [TrackingProperty(nameof(MonthExperience))]
            public double MonthExperience { get; set; }
        }
    }
}