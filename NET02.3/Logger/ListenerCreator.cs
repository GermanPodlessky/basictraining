﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Listener;

namespace Logger
{
    /// <summary>
    /// The class to create a listener.
    /// </summary>
    public static class ListenerCreator
    {
        /// <summary>
        /// The static method creates listeners.
        /// </summary>
        /// <param name="listeners">Collection of listeners.</param>
        /// <returns>Enumerator listeners.</returns>
        public static IEnumerable<IListener> Create(Dictionary<string, Dictionary<string, string>> listeners)
        {
            foreach (var valuePair in listeners)
            {
                var assemblyName = new AssemblyName(valuePair.Key.Split('.')[0]);

                Type type;
                try
                {
                    var assembly = Assembly.Load(assemblyName);
                    type = assembly.GetType(valuePair.Key, true);
                }
                catch
                {
                    continue;
                }

                yield return (IListener) Activator.CreateInstance(type, valuePair.Value);
            }
        }
    }
}