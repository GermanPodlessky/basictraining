﻿using System.Configuration;

namespace Logger
{
    /// <summary>
    /// The class to describe the log section in the configuration file.
    /// </summary>
    class LoggerSection : ConfigurationSection
    {
        [ConfigurationProperty("startupListeners", IsRequired = true)]
        public ListenerElements Listeners => (ListenerElements) base["startupListeners"];

        [ConfigurationProperty("logLevel", IsRequired = true)]
        public string LogLevel => this["logLevel"] as string;
    }
}