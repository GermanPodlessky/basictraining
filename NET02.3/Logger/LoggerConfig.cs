﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using Listener;

namespace Logger
{
    /// <summary>
    /// The singleton class to describe the logger configuration.
    /// </summary>
    public class LoggerConfig
    {
        private static readonly Lazy<LoggerConfig> _config = new Lazy<LoggerConfig>(() => new LoggerConfig());

        public static LoggerConfig GetInstance()
        {
            return _config.Value;
        }

        /// <summary>
        /// The method gets a section of the "logger" from the imported
        /// configuration file, and from the section gets information
        /// about the type of listeners.
        /// </summary>
        /// <returns></returns>
        public (LogLevel, Dictionary<string, Dictionary<string, string>>) GetListenersType()
        {
            var loggerSection = (LoggerSection) ConfigurationManager.GetSection("logger");
            if (loggerSection == null)
            {
                throw new ConfigurationErrorsException();
            }

            var level = (LogLevel) Enum.Parse(typeof(LogLevel), loggerSection.LogLevel);
            var listenersInfo = new Dictionary<string, Dictionary<string, string>>();
            var listeners = loggerSection.Listeners;
            for (var i = 0; i < listeners?.Count; i++)
            {
                var assemblyType = listeners[i].Assembly + "." + listeners[i].Type;

                var listenerParams =
                    (NameValueCollection) ConfigurationManager.GetSection("listeners/" + listeners[i].Type);

                var listenerKeyValue =
                    (listenerParams?.AllKeys).ToDictionary(param => param, param => listenerParams[param]);

                listenersInfo.Add(assemblyType, listenerKeyValue);
            }

            return (level, listenersInfo);
        }

        private LoggerConfig()
        {
        }
    }
}