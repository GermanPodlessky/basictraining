﻿using System.Configuration;

namespace Logger
{
    /// <summary>
    /// The class to describe the element of the listener.
    /// </summary>
    class ListenerElement : ConfigurationElement
    {
        [ConfigurationProperty("assembly", IsRequired = true)]
        public string Assembly => base["assembly"] as string;

        [ConfigurationProperty("type", IsRequired = true)]
        public string Type => base["type"] as string;
    }
}