﻿using System;
using System.Configuration;

namespace Logger
{
    /// <summary>
    /// The class to describe the collection listeners.
    /// </summary>
    [ConfigurationCollection(typeof(ListenerElement))]
    class ListenerElements : ConfigurationElementCollection
    {
        public override ConfigurationElementCollectionType CollectionType =>
            ConfigurationElementCollectionType.BasicMap;

        public ListenerElement this[int i] => BaseGet(i) as ListenerElement;

        protected override string ElementName { get; } = "listener";

        protected override ConfigurationElement CreateNewElement()
        {
            return new ListenerElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as ListenerElement)?.Assembly ?? throw new InvalidOperationException();
        }
    }
}