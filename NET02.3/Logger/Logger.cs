﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Listener;
using TrackingAttribute;

namespace Logger
{
    /// <summary>
    /// The class to describe the logger.
    /// </summary>
    public class Logger
    {
        private readonly List<IListener> _listeners;
        private readonly LogLevel _logLevel;

        public Logger()
        {
            var config = LoggerConfig.GetInstance();
            var (logLevel, listenersInfo) = config.GetListenersType();
            (_listeners, _logLevel) = (ListenerCreator.Create(listenersInfo).ToList(), logLevel);
        }

        /// <summary>
        /// The method for writing logs.
        /// </summary>
        /// <param name="logLevel">Minimum logging level.</param>
        /// <param name="message">Message for log.</param>
        public void Write(LogLevel logLevel, string message)
        {
            if (logLevel < _logLevel)
            {
                return;
            }

            foreach (var listener in _listeners)
            {
                listener.Write(logLevel, message);
            }
        }

        /// <summary>
        /// The method parses an arbitrary object into an object type, fields, properties, and transmits information for the log.
        /// </summary>
        /// <typeparam name="T">Arbitrary object type.</typeparam>
        /// <param name="type">Arbitrary object.</param>
        public void Track<T>(T type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            var entityAttribute = Attribute.GetCustomAttribute(type.GetType(), typeof(TrackingEntityAttribute));

            if (entityAttribute == null)
            {
                return;
            }

            var builder = new StringBuilder();

            foreach (var member in type.GetType()
                .GetMembers(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static))
            {
                TrackingPropertyAttribute temp;
                if ((temp = member.GetCustomAttribute<TrackingPropertyAttribute>()) == null)
                {
                    continue;
                }

                builder.Append(
                    $"{temp.Name} = {(member is FieldInfo field ? field.GetValue(type) : member is PropertyInfo prop ? prop.GetValue(type) : null)}");
            }

            Write(_logLevel, builder.ToString());
        }
    }
}